import numpy as np
import pandas as pd
import sys
import glob
import argparse


class PPIforFFmatrix:

    def __init__(self):

        parser = argparse.ArgumentParser()
        parser.add_argument("--ifie", help="ffmatrixファイル(IFIE)を指定して読み込む")
        parser.add_argument("--es", help="ffmatrixファイル(ES)を指定して読み込む")
        parser.add_argument("--ex", help="ffmatrixファイル(EX)を指定して読み込む")
        parser.add_argument("--ct", help="ffmatrixファイル(CT)を指定して読み込む")
        parser.add_argument("--di", help="ffmatrixファイル(DI)を指定して読み込む")
        parser.add_argument("--dist", help="ffmatrixファイル(Distance)を指定して読み込む")
        parser.add_argument("--distfilter", default=5, help="1vs1のPIEDAテーブルを作成する際の距離の閾値を指定")
        parser.add_argument("--outname", default="result", help="出力ファイルの名前を指定")
        parser.add_argument("--correctlabel", action="store_true", help="残基のラベルを修正")
        self.args = parser.parse_args()


    def readfile(self):

        # IFIE ファイル判定 #
        self.ifiefile = self._check_dirfiles(self.args.ifie, "*total-ffmatrix.csv")
        print("IFIEファイル : ", self.ifiefile, "が読みこまれました。")

        # ES ファイル判定 #
        self.esfile = self._check_dirfiles(self.args.es, "*-ES-ffmatrix.csv")
        print("ESファイル : ", self.esfile, "が読みこまれました。")

        # EX ファイル判定 #
        self.exfile = self._check_dirfiles(self.args.ex, "*-EX-ffmatrix.csv")
        print("EXファイル : ", self.exfile, "が読みこまれました。")

        # CT ファイル判定 #
        self.ctfile = self._check_dirfiles(self.args.ct, "*-CT-ffmatrix.csv")
        print("CTファイル : ", self.ctfile, "が読みこまれました。")

        # DI ファイル判定 #
        self.difile = self._check_dirfiles(self.args.di, "*corr-ffmatrix.csv")
        print("DIファイル : ", self.difile, "が読みこまれました。") 

        # Distance ファイル判定 #
        self.distfile = self._check_dirfiles(self.args.dist, "*-Distance-ffmatrix.csv")
        print("Distanceファイル : ", self.distfile, "が読みこまれました。") 

        # PIEDAクラスのインスタンス作成 #
        self.ifie = PiedaTerm(self.ifiefile, termtype="IFIE", correct_label=self.args.correctlabel)
        self.es = PiedaTerm(self.esfile, termtype="ES", correct_label=self.args.correctlabel)
        self.ex = PiedaTerm(self.exfile, termtype="EX", correct_label=self.args.correctlabel)
        self.ct = PiedaTerm(self.ctfile, termtype="CT", correct_label=self.args.correctlabel)
        self.di = PiedaTerm(self.difile, termtype="DI", correct_label=self.args.correctlabel)
        self.dist = PiedaTerm(self.distfile, termtype="Distance", correct_label=self.args.correctlabel)
        self.terms = [self.ifie, self.es, self.ex, self.ct, self.di, self.dist]

        # PIEDAインスタンスが同一のカラム・インデックスを持つか判定(解析対象系が同じか確認) #
        ifie_column, ifie_index = self.ifie.get_label()

        for terms in self.terms:

            tmp_column, tmp_index = terms.get_label()

            if tmp_column != ifie_column:

                print("残基のカラムが一致しません。\nCSVファイルを確認してください。")
                sys.exit()

            elif tmp_index != ifie_index:

                print("残基のインデックスが一致しません。\nCSVファイルを確認してください。")
                sys.exit()

        # 出力ファイルの名前を指定 #
        self.outname = self.args.outname


    # 読み込むファイルの判定 #
    def _check_dirfiles(self, flagfile, query):

        if flagfile != None:

            if flagfile in glob.glob("*.csv"):

                return flagfile

            else:

                print("指定されたファイルが見つかりません。")
                sys.exit()

        else:

            dir_files = glob.glob(query)

            if len(dir_files) == 1:

                return dir_files[0]

            elif len(dir_files) == 0:

                print("ディレクトリ内に必要なファイルが見つかりません。\nIFIE, ES, EX, CT, DI, Distanceファイルが必要です。")
                sys.exit()
        
            else:

                print("ディレクトリ内にファイルの候補が複数あります。フラグで指定してください。")
                sys.exit()


    # N:N SUM, N:1 SUM Tableを作成 #
    def get_sum(self):

        sumA = pd.concat([self.ifie.sumA(), self.es.sumA(), self.ex.sumA(), self.ct.sumA(), self.di.sumA()], axis=1)
        sumA.columns = ["IFIE", "ES", "EX", "CT", "DI"]
        sumB = pd.concat([self.ifie.sumB(), self.es.sumB(), self.ex.sumB(), self.ct.sumB(), self.di.sumB()], axis=1) 
        sumB.columns = ["IFIE", "ES", "EX", "CT", "DI"]

        sumAll = pd.DataFrame([self.ifie.sumAll(), self.es.sumAll(), self.ex.sumAll(), self.ct.sumAll(), self.di.sumAll()], 
                              index = ["IFIE", "ES", "EX", "CT", "DI"])


        sumA.to_csv(str(self.outname) + "_SUM_1vsN_column.csv")
        print(str(self.outname) + "_SUM_1vsN_column.csv が出力されました。")

        sumB.to_csv(str(self.outname) + "_SUM_1vsN_index.csv")
        print(str(self.outname) + "_SUM_1vsN_index.csv が出力されました。")

        sumAll.to_csv(str(self.outname) + "_SUM_All.csv")
        print(str(self.outname) + "_SUM_All.csv が出力されました。")


    def get_sum_filtered(self):
        
        query_column, query_index = self.dist.dist_filter_NvsN(self.args.distfilter)

        fil_ifie = self.ifie.df.drop(columns=query_column).drop(index=query_index)
        fil_es = self.es.df.drop(columns=query_column).drop(index=query_index)
        fil_ex = self.ex.df.drop(columns=query_column).drop(index=query_index)
        fil_ct = self.ct.df.drop(columns=query_column).drop(index=query_index)
        fil_di = self.di.df.drop(columns=query_column).drop(index=query_index)

        sumA = pd.concat([fil_ifie.sum(), fil_es.sum(), fil_ex.sum(), fil_ct.sum(), fil_di.sum()], axis=1)
        sumA.columns = ["IFIE", "ES", "EX", "CT", "DI"]
        sumB = pd.concat([fil_ifie.sum(axis=1), fil_es.sum(axis=1), fil_ex.sum(axis=1), fil_ct.sum(axis=1), fil_di.sum(axis=1)], axis=1) 
        sumB.columns = ["IFIE", "ES", "EX", "CT", "DI"]

        sumAll = pd.DataFrame([fil_ifie.sum().sum(), fil_es.sum().sum(), fil_ex.sum().sum(), fil_ct.sum().sum(), fil_di.sum().sum()], 
                              index = ["IFIE", "ES", "EX", "CT", "DI"])


        sumA.to_csv(str(self.outname) + "_SUM_1vsN_column_filtered.csv")
        print(str(self.outname) + "_SUM_1vsN_column_filterd.csv が出力されました。")

        sumB.to_csv(str(self.outname) + "_SUM_1vsN_index_filtered.csv")
        print(str(self.outname) + "_SUM_1vsN_index_filterd.csv が出力されました。")

        sumAll.to_csv(str(self.outname) + "_SUM_All_filtered.csv")
        print(str(self.outname) + "_SUM_All_filtered.csv が出力されました。")


    # 1:1 のPIEDA Tableを作成 #
    def get_1vs1(self):

        querylist = self.dist.dist_filter_1vs1(self.args.distfilter)

        results = []
        for term in self.terms:

            results.append(term.get_values(querylist))

        table = pd.concat(results, axis=1)
        table = table.loc[:,~table.columns.duplicated()]
        table.to_csv(str(self.outname) + "_PIEDA_1vs1.csv")
        print(str(self.outname) + "_PIEDA_1vs1.csv が出力されました。")


# PIEDA項のクラス#
class PiedaTerm:


    def __init__(self, pieda_term, termtype, correct_label):

        self.df = pd.read_csv(pieda_term)
        self.df.set_index(self.df.columns[0], inplace=True)

        if correct_label:

            self._correct_label()

        self.get_label()
        self.type = termtype


    # 残基のラベルを修正 #
    def _correct_label(self):

        pre_column = self.df.columns.tolist()
        pre_index = self.df.index.tolist()

        column = []
        index = []

        for label in pre_column:

            label = label.split("(")[0].capitalize()
            column.append(label)

        for label in pre_index:

            label = label.split("(")[0].capitalize()
            index.append(label)

        self.df.columns = column
        self.df.index = index


    # 残基のカラムとインデックスを出力 #
    def get_label(self):

        self.column = self.df.columns.tolist()
        self.index = self.df.index.tolist()

        return self.column, self.index


    # 1:N SUMを算出(カラム) #
    def sumA(self):

        return self.df.sum()


    # 1:N SUMを算出(インデックス) #
    def sumB(self):

        return self.df.sum(axis=1)


    # N:N SUMを算出 #
    def sumAll(self):

        return self.df.sum().sum()       


    # ([カラム番号], [インデックス番号]) で指定された要素を抽出 #
    def get_values(self, querylist):

        result = []
        df = self.df.to_numpy()

        for i in range(len(querylist[0])):

            index = querylist[0][i]
            column = querylist[1][i]
            value = df[index][column]
            residue_index = self.index[index]
            residue_column = self.column[column]

            result.append([residue_column, residue_index, value])

        return pd.DataFrame(result, columns=["Residue_column", "Residue_index", self.type])


    def dist_filter_NvsN(self, threshold):

        if self.type != "Distance":

            print("DistanceフィルターはDistance Typeにしか適用できません。")
            sys.exit()

        querymap = self.df[(self.df < float(threshold))]
        query_column = querymap.isnull().all()
        query_index = querymap.isnull().all(axis=1)

        query_column = query_column[query_column == True].index.to_list()
        query_index = query_index[query_index == True].index.to_list()

        return query_column, query_index


    # 条件に合う距離を持つ、カラム・インデックス番号のリストを生成 ([カラム番号],[インデックス番号]) #
    def dist_filter_1vs1(self, threshold):

        if self.type != "Distance":

            print("DistanceフィルターはDistance Typeにしか適用できません。")
            sys.exit()

        df = self.df.to_numpy()
        querylist = np.where(df < float(threshold))

        return querylist


# Main #
if __name__ == "__main__":

    analizer = PPIforFFmatrix()
    analizer.readfile()
    analizer.get_sum()
    analizer.get_sum_filtered()
    analizer.get_1vs1()
